/**
 * Created by richard on 17/10/16.
 */
/*global angular, $*/
'use strict';
require('./vendor/jquery.visible');

//noinspection JSUnusedGlobalSymbols,JSUnresolvedFunction
angular.module('bitcraft-server-log')
    .component('bitcraftServerLog', {
        templateUrl: './js/bitcraft-server-log/server-log.template.html',
        controller: [
            '$cookies', '$window', '$anchorScroll', '$timeout', 'Faye', 'ServerLog',
            function ($cookies, $window, $anchorScroll, $timeout, Faye, ServerLog) {
                var self = this;
                // logRepeatEndPending is set to true in shouldScroll function and reset to false
                // in onRepeatDone. Once we get a message from the faye service, check if we should
                // scroll and wait until angular trigger the onRepeatDone to reset the state.
                var logRepeatEndPending = false;
                /** @type {Promise|null} */
                var logRefreshPromise = null;
                /** @type {Promise|null} */
                var scrollToPromise = null;
                /** @type {Array} */
                var pendingLogs = [];
                /** @type {function()} */
                var flushLog;
                /** @type {number} */
                var logEntryId = 0;
                /** @type {{unsubscribe: function}} */
                var fayeOnConnectSubscription;
                /** @type {{unsubscribe: function}} */
                var fayeOnDisconnectSubscription;
                /** @type {string} */
                var searchCookie = 'log_search';
                /** @type {string} */
                var filterCookie = 'log_filter';

                function initStickyPart() {
                    self.anchorTop = $('#scroller-anchor');
                    self.scroller = $('#scroller');
                    self.scroller.width($('#scroller-width').width() + 30);

                    angular.element($window).bind('resize', function () {
                        self.scroller.width($('#scroller-width').width() + 30);
                    });

                    angular.element($window).bind('scroll', function () {
                        var st = $(window).scrollTop() + 50;
                        var ot = self.anchorTop.offset().top;
                        if (st > ot) {
                            self.scroller.addClass('scroller-fixed').removeClass('scroller-relative');
                        } else {
                            if (st <= ot) {
                                self.scroller.addClass('scroller-relative').removeClass('scroller-fixed');
                            }
                        }
                    });
                }

                function maybeRefreshLog() {
                    if (logRefreshPromise === null && pendingLogs.length > 0) {
                        logRefreshPromise = $timeout(flushLog, self.options.flushDelay);
                    }
                }

                function shouldScroll() {
                    if (self.autoScroll && !logRepeatEndPending) {
                        logRepeatEndPending = true;
                    }
                }

                flushLog = function () {
                    if (logRefreshPromise !== null) {
                        logRefreshPromise = null;
                    }
                    if (pendingLogs.length > 0) {
                        shouldScroll();
                        self.messages.push.apply(self.messages, pendingLogs);
                        pendingLogs = [];
                    }
                };

                function tryAppendMessage(message) {
                    try {
                        if (self.options.levels[message.level].enabled && ServerLog.isMessageVisible(self.channel, message)) {
                            message.id = logEntryId;
                            logEntryId += 1;
                            pendingLogs.push(message);
                            maybeRefreshLog();
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }

                function readMessages() {
                    var i;
                    var storedMessages = ServerLog.getMessages(self.channel);
                    for (i = 0; i < storedMessages.length; i = i + 1) {
                        tryAppendMessage(storedMessages[i]);
                    }
                }

                function onFayeStateChanged() {
                    self.state = Faye.getState() === Faye.STATES.CONNECTED ? 'SERVER_LOG.CONNECTED' : 'SERVER_LOG.DISCONNECTED';
                }

                /**
                 * return true to keep a message
                 * @param {string} searchInput
                 * @returns {function}
                 */
                function generateDisplayFilter(searchInput) {
                    return function (message) {
                        if (!message.data) {
                            return message.header.indexOf(searchInput) !== -1;
                        }
                        return message.header.indexOf(searchInput) !== -1 || message.data.indexOf(searchInput) !== -1;
                    };
                }

                function reset() {
                    logEntryId = 0;
                    self.messages = [];
                    pendingLogs = [];
                    if (logRefreshPromise !== null) {
                        $timeout.cancel(logRefreshPromise);
                        logRefreshPromise = null;
                    }
                }

                function execSearch() {
                    if (self.currentSearchFunction) {
                        ServerLog.removeDisplayFilterFunction(self.channel, self.currentSearchFunction);
                    }
                    if (self.searchInput) {
                        self.currentSearchFunction = generateDisplayFilter(self.searchInput);
                        ServerLog.pushDisplayFilterFunction(self.channel, self.currentSearchFunction);
                        $cookies.put(searchCookie, self.searchInput);
                    } else {
                        $cookies.remove(searchCookie);
                    }

                    reset();
                    readMessages();
                }

                this.$onInit = function () {
                    var cookie;
                    var i;
                    if (self.channel[0] !== '/') {
                        self.channel = '/' + self.channel;
                    }

                    self.options = ServerLog.getOptions(self.channel);
                    self.autoScroll = false;

                    onFayeStateChanged();
                    fayeOnConnectSubscription = Faye.onConnect(onFayeStateChanged);
                    fayeOnDisconnectSubscription = Faye.onDisconnect(onFayeStateChanged);

                    // generates a random anchor of 8 characters length
                    // in case we have multiple faye-log component on the same page
                    self.anchorName = Math.random().toString(36).substring(7, 15);

                    initStickyPart();

                    self.searchInput = $cookies.get(searchCookie);
                    cookie = $cookies.get(filterCookie);
                    if (cookie) {
                        cookie.split('\\_')
                            .some(function (e) {
                                if (e.indexOf(self.channel) === 0) {
                                    self.filterInput = e.replace(/\\\|/g, '\n').substr(self.channel.length + 1);
                                    return true; // break
                                }
                                return false;
                            });
                    }

                    execSearch(); // readMessages(); is done by execSearch();
                    ServerLog.onMessageReceived(self.channel, tryAppendMessage);
                };

                this.$onDestroy = function () {
                    ServerLog.offMessageReceived(self.channel);
                    if (logRefreshPromise !== null) {
                        $timeout.cancel(logRefreshPromise);
                        logRefreshPromise = null;
                    }
                    if (scrollToPromise !== null) {
                        $timeout.cancel(scrollToPromise);
                        scrollToPromise = null;
                    }
                    if (fayeOnConnectSubscription) {
                        fayeOnConnectSubscription.unsubscribe();
                    }
                    if (fayeOnDisconnectSubscription) {
                        fayeOnDisconnectSubscription.unsubscribe();
                    }
                    if (self.currentSearchFunction) {
                        ServerLog.removeDisplayFilterFunction(self.channel, self.currentSearchFunction);
                    }
                };

                self.search = function () {
                    if (self.searchTimeout) {
                        clearTimeout(self.searchTimeout);
                    }
                    self.searchTimeout = setTimeout(execSearch, self.options.searchDelay);
                };

                self.applyFilter = function () {
                    var currentCookie = $cookies.get(filterCookie);
                    var notFound = true;
                    var i;

                    if (currentCookie) {
                        currentCookie = currentCookie.split('\\_');

                        for (i = 0; i < currentCookie.length; i += 1) {
                            if (currentCookie[i].indexOf(self.channel) === 0) {
                                if (self.filterInput) {
                                    currentCookie[i] = self.channel + '\\|' + self.filterInput.replace(/\n/g, '\\|');
                                } else {
                                    currentCookie.splice(i, 1);
                                }
                                notFound = false;
                                break;
                            }
                        }
                    }

                    if (notFound && self.filterInput) {
                        if (!currentCookie) {
                            currentCookie = [];
                        }
                        currentCookie.push(self.channel + '\\|' + self.filterInput.replace(/\n/g, '\\|'));
                    }

                    $cookies.put(filterCookie, currentCookie.join('\\_'));
                    ServerLog.updateCustomFiltering(self.channel);
                    reset();
                    readMessages();
                };

                self.clear = function () {
                    ServerLog.clearMessages(self.channel);
                    reset();
                };

                self.toggleScroll = function () {
                    self.autoScroll = !self.autoScroll;
                };

                self.toggleLevel = function (level) {
                    self.options.levels[level].enabled = !self.options.levels[level].enabled;
                    reset();
                    readMessages();
                };

                self.onRepeatDone = function () {
                    // trigger an auto scroll if the anchor was visible
                    if (self.autoScroll) {
                        // scroll to the last displayed alert
                        // use $timeout instead of $scope.$evalAsync to ensure the DOM has
                        // been rendered
                        // see http://blogs.microsoft.co.il/choroshin/2014/04/08/angularjs-postdigest-vs-timeout-when-dom-update-is-needed/
                        scrollToPromise = $timeout(function () {
                            $anchorScroll(self.anchorName);
                            logRepeatEndPending = false;
                            scrollToPromise = null;
                            maybeRefreshLog();
                        });
                    } else {
                        logRepeatEndPending = false;
                        maybeRefreshLog();
                    }
                };
            }
        ],
        bindings: {
            channel: '<'
        }
    });

/*global angular*/
'use strict';

angular.module('bitcraft-server-log')
    .factory('ServerLog', [
        '$rootScope', '$cookies', 'Faye',
        function ($rootScope, $cookies, Faye) {
            /** @type {Object.<string, ChannelConfig>} */
            var config = {};
            /** @type {Object.<string, Object>} */
            var subscriptions = {};
            /** @type {String} */
            var filterCookie = 'log_filter'; // log_filter=/channel1\|filter1\|filter2\_channel2\|filter1\|filter2...

            /**
             * @param {string} channel
             */
            function addSubscription(channel) {
                if (!$rootScope.messages) {
                    $rootScope.messages = {};
                }

                $rootScope.messages[channel] = [];
                subscriptions[channel] = Faye.subscribe(channel, function (context, message) {
                    /** @type ChannelConfig */
                    var channelConfig = config[channel];
                    var i;

                    // format
                    for (i = 0; i < channelConfig.formatList.length; i += 1) {
                        message = channelConfig.formatList[i](message);
                    }

                    // filter must return true to keep a message
                    for (i = 0; i < channelConfig.filterList.length; i += 1) {
                        if (!channelConfig.filterList[i](message)) {
                            return;
                        }
                    }

                    // push
                    $rootScope.messages[channel].push(message);
                    if ($rootScope.messages[channel].length > channelConfig.limit) {
                        $rootScope.messages[channel].shift();
                    }

                    // call the listeners
                    for (i = 0; i < channelConfig.listenerList.length; i += 1) {
                        channelConfig.listenerList[i](message);
                    }
                }, true);
            }

            /**
             * @param {string} channel
             */
            function createChannel(channel) {
                if (!config[channel]) {
                    addSubscription(channel);
                    config[channel] = {
                        filterList: [],
                        formatList: [],
                        displayFilterList: [],
                        listenerList: [],
                        limit: 1000,
                        options: {
                            levels: {
                                'SERVER_LOG.TRACE': {
                                    enabled: true,
                                    'background-color': '#dff0d8',
                                    'border-color': '#d0e9c6',
                                    color: '#3c763d'
                                },
                                'SERVER_LOG.DEBUG': {
                                    enabled: true,
                                    'background-color': '#a5f3e4',
                                    'border-color': '#62f5e7',
                                    color: '#4b89a7'
                                },
                                'SERVER_LOG.INFO': {
                                    enabled: true,
                                    'background-color': '#d9edf7',
                                    'border-color': '#bcdff1',
                                    color: '#31708f'
                                },
                                'SERVER_LOG.WARNING': {
                                    enabled: true,
                                    'background-color': '#fcf8e3',
                                    'border-color': '#faebcc',
                                    color: '#8a6d3b'
                                },
                                'SERVER_LOG.ERROR': {
                                    enabled: true,
                                    'background-color': '#f2dede',
                                    'border-color': '#ebcccc',
                                    color: '#a94442'
                                },
                                'SERVER_LOG.FATAL': {
                                    enabled: true,
                                    'background-color': '#3c3232',
                                    'border-color': '#756060',
                                    color: '#f3dada'
                                }
                            },
                            flushDelay: 200,
                            searchDelay: 200,
                            showStatus: true,
                            showClearMessages: true,
                            showAutoScroll: true,
                            showLevels: true,
                            showSearch: true,
                            showCustomFilter: true
                        }
                    };
                }
            }

            /**
             * @param {string} channel
             * @param {ChannelOptions} options
             */
            function setOptions(channel, options) {
                var key;
                if (!config[channel]) {
                    createChannel(channel);
                }

                for (key in config[channel].options) {
                    if (config[channel].options.hasOwnProperty(key) && options[key] !== undefined && options[key] !== null) {
                        config[channel].options[key] = options[key];
                    }
                }
            }

            /**
             * @param {string} channel
             * @returns {ChannelOptions}
             */
            function getOptions(channel) {
                return config[channel].options;
            }

            /**
             * @param {string} channel
             * @param {number} limit
             */
            function setLimit(channel, limit) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].limit = limit;
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             */
            function pushFilteringFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].filterList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeFilteringFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].filterList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].formatList[i], fn)) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].filterList[i] === fn) {
                            config[channel].filterList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object): object} fn
             */
            function pushFormatingFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].formatList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): object} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeFormatingFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].formatList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].formatList[i], fn)) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].formatList[i] === fn) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             */
            function pushDisplayFilterFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].displayFilterList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeDisplayFilterFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].displayFilterList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].displayFilterList[i], fn)) {
                            config[channel].displayFilterList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].displayFilterList[i] === fn) {
                            config[channel].displayFilterList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object)} fn
             */
            function onMessageReceived(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].listenerList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object)} fn
             */
            function offMessageReceived(channel, fn) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].listenerList.length; i += 1) {
                    if (config[channel].listenerList[i] === fn) {
                        config[channel].listenerList.splice(i, 1);
                        return;
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {Object} message
             * @returns {boolean}
             */
            function isMessageVisible(channel, message) {
                var i;
                /** @type {DisplayFilterList} */
                var displayFilterList;

                if (!config[channel]) {
                    return true;
                }

                displayFilterList = config[channel].displayFilterList;

                for (i = 0; i < displayFilterList.length; i += 1) {
                    if (displayFilterList[i](message) === false) {
                        return false;
                    }
                }

                return true;
            }

            /**
             * @param {string} channel
             * @returns {Array<Object>}
             */
            function getMessages(channel) {
                if (!config[channel]) {
                    return null;
                }

                return $rootScope.messages[channel];
            }

            /**
             * @param {string} channel
             */
            function clearMessages(channel) {
                if (!config[channel]) {
                    return;
                }

                $rootScope.messages[channel] = [];
            }

            /**
             * Remove all subscriptions
             */
            function cleanUp() {
                var key;
                for (key in subscriptions) {
                    if (subscriptions.hasOwnProperty(key)) {
                        subscriptions[key].unsubscribe(true);
                    }
                }
            }

            /**
             * @param {Array<string>} e
             * @returns {Function}
             */
            function generateCustomFilterFunction(e) {
                return function (message) {
                    var i;
                    for (i = 1; i < e.length; i += 1) {
                        if (message.header.indexOf(e[i]) > -1 || (message.data && message.data.indexOf(e[i]) > -1)) {
                            return false;
                        }
                    }

                    return true;
                };
            }

            /**
             * @param {string=} channel
             */
            function updateCustomFiltering(channel) {
                var cookie = $cookies.get(filterCookie);
                if (cookie) {
                    cookie.split('\\_')
                        .map(function (e) {
                            return e.split('\\|');
                        })
                        .some(function (e) {
                            if (channel && channel !== e[0]) {
                                return false;
                            }

                            if (!config[e[0]]) {
                                createChannel(e[0]);
                            }

                            if (config[e[0]].customFilteringFunction) {
                                removeFilteringFunction(e[0], config[e[0]].customFilteringFunction);
                            }

                            config[e[0]].customFilteringFunction = generateCustomFilterFunction(e);
                            $rootScope.messages[e[0]] = $rootScope.messages[e[0]].filter(config[e[0]].customFilteringFunction);
                            pushFilteringFunction(e[0], config[e[0]].customFilteringFunction);
                            return true; //break
                        });
                }
            }

            updateCustomFiltering();

            return {
                createChannel: createChannel,
                setLimit: setLimit,
                setOptions: setOptions,
                getOptions: getOptions,
                updateCustomFiltering: updateCustomFiltering,

                pushFilteringFunction: pushFilteringFunction,
                removeFilteringFunction: removeFilteringFunction,

                pushFormatingFunction: pushFormatingFunction,
                removeFormatingFunction: removeFormatingFunction,

                pushDisplayFilterFunction: pushDisplayFilterFunction,
                removeDisplayFilterFunction: removeDisplayFilterFunction,

                onMessageReceived: onMessageReceived,
                offMessageReceived: offMessageReceived,

                isMessageVisible: isMessageVisible,

                getMessages: getMessages,
                clearMessages: clearMessages,

                cleanUp: cleanUp
            };
        }
    ]);

/**
 * @typedef {Array<function(object): boolean>} FilterList
 */

/**
 * @typedef {Array<function(object): object>} FormatList
 */

/**
 * @typedef {Array<function(object): boolean>} DisplayFilterList
 */

/**
 * @typedef {Array<function(object)>} ListenerList
 */

/**
 * @typedef {Object} ChannelOptions
 * @property {Array<string>} levels
 * @property {number} flushDelay
 * @property {number} searchDelay
 * @property {boolean} showStatus
 * @property {boolean} showClearMessages
 * @property {boolean} showAutoScroll
 * @property {boolean} showLevels
 * @property {boolean} showSearch
 * @property {boolean} showCustomFilter
 */

/**
 * @typedef {Object} ChannelConfig
 * @property {FilterList} filterList
 * @property {FormatList} formatList
 * @property {DisplayFilterList} displayFilterList
 * @property {ListenerList} listenerList
 * @property {Number} limit
 * @property {function|undefined} customFilteringFunction
 * @property {ChannelOptions} options
 */

/*global angular*/
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-server-log')
    .component('bitcraftServerLogEntry', {
        templateUrl: './js/bitcraft-server-log-entry/server-log-entry.template.html',
        controller: [
            '$scope',
            function ($scope) {
                this.$onInit = function () {
                    $scope.showAll = false;
                    $scope.$applyAsync();
                };

                this.toggleDisplay = function () {
                    $scope.showAll = !$scope.showAll;
                };
            }
        ],
        bindings: {
            entry: '=',
            colors: '<'
        }
    });
// background-color: #a5f3e4;
// border-color: #62f5e7;
// color: #4b89a7;
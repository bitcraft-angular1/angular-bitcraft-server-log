(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
require('./server-log.module.js');
require('./server-log.component.js');
require('./server-log.service.js');
require('./server-log-entry');

},{"./server-log-entry":2,"./server-log.component.js":4,"./server-log.module.js":5,"./server-log.service.js":6}],2:[function(require,module,exports){
'use strict';
require('./server-log-entry.component.js');

},{"./server-log-entry.component.js":3}],3:[function(require,module,exports){
/*global angular*/
'use strict';

var template = '' +
'<div class="alert alert-block" ng-style="::$ctrl.colors">' +
'    <div>' +
'        <i ng-if="::$ctrl.entry.dataDetails" ng-click="$ctrl.toggleDisplay()" ng-class="showAll ? \'fa-minus-square-o\' : \'fa-plus-square-o\'" class="fa"></i>' +
'        <span>{{::$ctrl.entry.header}}</span>' +
'    </div>' +
'    <div ng-show="showAll">' +
'        <div style="padding-left: 10px" ng-repeat="value in ::$ctrl.entry.dataDetails">{{::value}}</div>' +
'    </div>' +
'</div>' +
'';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-server-log')
    .component('bitcraftServerLogEntry', {
        template: template,
        controller: [
            '$scope',
            function ($scope) {
                this.$onInit = function () {
                    $scope.showAll = false;
                    $scope.$applyAsync();
                };

                this.toggleDisplay = function () {
                    $scope.showAll = !$scope.showAll;
                };
            }
        ],
        bindings: {
            entry: '=',
            colors: '<'
        }
    });
// background-color: #a5f3e4;
// border-color: #62f5e7;
// color: #4b89a7;
},{}],4:[function(require,module,exports){
/**
 * Created by richard on 17/10/16.
 */
/*global angular, $*/
'use strict';

var template = '' +
'<!--suppress CssUnusedSymbol -->' +
'<style>' +
'    #scroller {' +
'        padding-bottom: 5px;' +
'        margin-left: -15px;' +
'        background-color: white;' +
'    }' +
'    .scroller-fixed {' +
'        position: fixed;' +
'        top: 50px;' +
'        padding-top: 25px;' +
'    }' +
'    .scroller-relative {' +
'        position: relative;' +
'        padding-top: 15px;' +
'    }' +
'    /*noinspection CssInvalidHtmlTagReference*/' +
'    bitcraft-server-log-entry {' +
'        word-wrap: break-word;' +
'    }' +
'    .alert {' +
'        margin-bottom: 1px ! important;' +
'    }' +
'</style>' +
'<div class="container-fluid" id="scroller-width">' +
'    <div id="scroller-anchor"></div>' +
'    <div id="scroller" class="scroller-relative">' +
'        <div translate="SERVER_LOG.STATE" translate-values="{ state: \'{{$ctrl.state}}\' }"  ng-if="$ctrl.options.showStatus"></div>' +
'        <div class="form-inline">' +
'            <div class="form-group">' +
'                <div class="form-group" ng-if="$ctrl.options.showClearMessages">' +
'                    <a class="form-control btn btn-default" type="button" ng-click="$ctrl.clear()" translate>SERVER_LOG.CLEAR</a>' +
'                </div>' +
'                <div class="form-group" data-toggle="buttons"  ng-if="$ctrl.options.showAutoScroll">' +
'                    <label ng-click="$ctrl.toggleScroll()" class="form-control btn btn-default">' +
'                        <input type="checkbox" autocomplete="off"><span translate>SERVER_LOG.AUTO_SCROLL</span>' +
'                    </label>' +
'                </div>' +
'                <div class="form-group" ng-if="$ctrl.options.showLevels">' +
'                    <div class="btn-group" data-toggle="buttons">' +
'                        <label ng-click="$ctrl.toggleLevel(level)" class="btn btn-default active" ng-repeat="(level, ignore) in $ctrl.options.levels">' +
'                            <input type="checkbox" checked autocomplete="off"><span translate>{{level}}</span>' +
'                        </label>' +
'                    </div>' +
'                </div>' +
'                <div class="form-group" ng-if="$ctrl.options.showSearch">' +
'                    <input class="form-control" type="text" placeholder="search" ng-change="$ctrl.search()" ng-model="$ctrl.searchInput">' +
'                </div>' +
'                <div class="form-group" ng-if="$ctrl.options.showCustomFilter">' +
'                    <textarea class="form-control" rows="1" placeholder="filter" ng-model="$ctrl.filterInput"></textarea>' +
'                </div>' +
'                <div class="form-group" ng-if="$ctrl.options.showCustomFilter">' +
'                    <a class="form-control btn btn-default" type="button" ng-click="$ctrl.applyFilter()" translate>Apply filter</a>' +
'                </div>' +
'            </div>' +
'        </div>' +
'    </div>' +
'    <div class="row">' +
'        <bitcraft-server-log-entry repeat-on-last-event="$ctrl.onRepeatDone"' +
'                                   ng-repeat="entry in $ctrl.messages track by entry.id"' +
'                                   entry="entry"' +
'                                   colors="$ctrl.options.levels[entry.level]">' +
'        </bitcraft-server-log-entry>' +
'    </div>' +
'    <!-- this anchor is used by the auto-scroll feature to stick to the last log item -->' +
'    <a name="{{$ctrl.anchorName}}"></a>' +
'</div>' +
'';

require('./vendor/jquery.visible');

//noinspection JSUnusedGlobalSymbols,JSUnresolvedFunction
angular.module('bitcraft-server-log')
    .component('bitcraftServerLog', {
        template: template,
        controller: [
            '$cookies', '$window', '$anchorScroll', '$timeout', 'Faye', 'ServerLog',
            function ($cookies, $window, $anchorScroll, $timeout, Faye, ServerLog) {
                var self = this;
                // logRepeatEndPending is set to true in shouldScroll function and reset to false
                // in onRepeatDone. Once we get a message from the faye service, check if we should
                // scroll and wait until angular trigger the onRepeatDone to reset the state.
                var logRepeatEndPending = false;
                /** @type {Promise|null} */
                var logRefreshPromise = null;
                /** @type {Promise|null} */
                var scrollToPromise = null;
                /** @type {Array} */
                var pendingLogs = [];
                /** @type {function()} */
                var flushLog;
                /** @type {number} */
                var logEntryId = 0;
                /** @type {{unsubscribe: function}} */
                var fayeOnConnectSubscription;
                /** @type {{unsubscribe: function}} */
                var fayeOnDisconnectSubscription;
                /** @type {string} */
                var searchCookie = 'log_search';
                /** @type {string} */
                var filterCookie = 'log_filter';

                function initStickyPart() {
                    self.anchorTop = $('#scroller-anchor');
                    self.scroller = $('#scroller');
                    self.scroller.width($('#scroller-width').width() + 30);

                    angular.element($window).bind('resize', function () {
                        self.scroller.width($('#scroller-width').width() + 30);
                    });

                    angular.element($window).bind('scroll', function () {
                        var st = $(window).scrollTop() + 50;
                        var ot = self.anchorTop.offset().top;
                        if (st > ot) {
                            self.scroller.addClass('scroller-fixed').removeClass('scroller-relative');
                        } else {
                            if (st <= ot) {
                                self.scroller.addClass('scroller-relative').removeClass('scroller-fixed');
                            }
                        }
                    });
                }

                function maybeRefreshLog() {
                    if (logRefreshPromise === null && pendingLogs.length > 0) {
                        logRefreshPromise = $timeout(flushLog, self.options.flushDelay);
                    }
                }

                function shouldScroll() {
                    if (self.autoScroll && !logRepeatEndPending) {
                        logRepeatEndPending = true;
                    }
                }

                flushLog = function () {
                    if (logRefreshPromise !== null) {
                        logRefreshPromise = null;
                    }
                    if (pendingLogs.length > 0) {
                        shouldScroll();
                        self.messages.push.apply(self.messages, pendingLogs);
                        pendingLogs = [];
                    }
                };

                function tryAppendMessage(message) {
                    try {
                        if (self.options.levels[message.level].enabled && ServerLog.isMessageVisible(self.channel, message)) {
                            message.id = logEntryId;
                            logEntryId += 1;
                            pendingLogs.push(message);
                            maybeRefreshLog();
                        }
                    } catch (error) {
                        console.log(error);
                    }
                }

                function readMessages() {
                    var i;
                    var storedMessages = ServerLog.getMessages(self.channel);
                    for (i = 0; i < storedMessages.length; i = i + 1) {
                        tryAppendMessage(storedMessages[i]);
                    }
                }

                function onFayeStateChanged() {
                    self.state = Faye.getState() === Faye.STATES.CONNECTED ? 'SERVER_LOG.CONNECTED' : 'SERVER_LOG.DISCONNECTED';
                }

                /**
                 * return true to keep a message
                 * @param {string} searchInput
                 * @returns {function}
                 */
                function generateDisplayFilter(searchInput) {
                    return function (message) {
                        if (!message.data) {
                            return message.header.indexOf(searchInput) !== -1;
                        }
                        return message.header.indexOf(searchInput) !== -1 || message.data.indexOf(searchInput) !== -1;
                    };
                }

                function reset() {
                    logEntryId = 0;
                    self.messages = [];
                    pendingLogs = [];
                    if (logRefreshPromise !== null) {
                        $timeout.cancel(logRefreshPromise);
                        logRefreshPromise = null;
                    }
                }

                function execSearch() {
                    if (self.currentSearchFunction) {
                        ServerLog.removeDisplayFilterFunction(self.channel, self.currentSearchFunction);
                    }
                    if (self.searchInput) {
                        self.currentSearchFunction = generateDisplayFilter(self.searchInput);
                        ServerLog.pushDisplayFilterFunction(self.channel, self.currentSearchFunction);
                        $cookies.put(searchCookie, self.searchInput);
                    } else {
                        $cookies.remove(searchCookie);
                    }

                    reset();
                    readMessages();
                }

                this.$onInit = function () {
                    var cookie;
                    var i;
                    if (self.channel[0] !== '/') {
                        self.channel = '/' + self.channel;
                    }

                    self.options = ServerLog.getOptions(self.channel);
                    self.autoScroll = false;

                    onFayeStateChanged();
                    fayeOnConnectSubscription = Faye.onConnect(onFayeStateChanged);
                    fayeOnDisconnectSubscription = Faye.onDisconnect(onFayeStateChanged);

                    // generates a random anchor of 8 characters length
                    // in case we have multiple faye-log component on the same page
                    self.anchorName = Math.random().toString(36).substring(7, 15);

                    initStickyPart();

                    self.searchInput = $cookies.get(searchCookie);
                    cookie = $cookies.get(filterCookie);
                    if (cookie) {
                        cookie.split('\\_')
                            .some(function (e) {
                                if (e.indexOf(self.channel) === 0) {
                                    self.filterInput = e.replace(/\\\|/g, '\n').substr(self.channel.length + 1);
                                    return true; // break
                                }
                                return false;
                            });
                    }

                    execSearch(); // readMessages(); is done by execSearch();
                    ServerLog.onMessageReceived(self.channel, tryAppendMessage);
                };

                this.$onDestroy = function () {
                    ServerLog.offMessageReceived(self.channel);
                    if (logRefreshPromise !== null) {
                        $timeout.cancel(logRefreshPromise);
                        logRefreshPromise = null;
                    }
                    if (scrollToPromise !== null) {
                        $timeout.cancel(scrollToPromise);
                        scrollToPromise = null;
                    }
                    if (fayeOnConnectSubscription) {
                        fayeOnConnectSubscription.unsubscribe();
                    }
                    if (fayeOnDisconnectSubscription) {
                        fayeOnDisconnectSubscription.unsubscribe();
                    }
                    if (self.currentSearchFunction) {
                        ServerLog.removeDisplayFilterFunction(self.channel, self.currentSearchFunction);
                    }
                };

                self.search = function () {
                    if (self.searchTimeout) {
                        clearTimeout(self.searchTimeout);
                    }
                    self.searchTimeout = setTimeout(execSearch, self.options.searchDelay);
                };

                self.applyFilter = function () {
                    var currentCookie = $cookies.get(filterCookie);
                    var notFound = true;
                    var i;

                    if (currentCookie) {
                        currentCookie = currentCookie.split('\\_');

                        for (i = 0; i < currentCookie.length; i += 1) {
                            if (currentCookie[i].indexOf(self.channel) === 0) {
                                if (self.filterInput) {
                                    currentCookie[i] = self.channel + '\\|' + self.filterInput.replace(/\n/g, '\\|');
                                } else {
                                    currentCookie.splice(i, 1);
                                }
                                notFound = false;
                                break;
                            }
                        }
                    }

                    if (notFound && self.filterInput) {
                        if (!currentCookie) {
                            currentCookie = [];
                        }
                        currentCookie.push(self.channel + '\\|' + self.filterInput.replace(/\n/g, '\\|'));
                    }

                    $cookies.put(filterCookie, currentCookie.join('\\_'));
                    ServerLog.updateCustomFiltering(self.channel);
                    reset();
                    readMessages();
                };

                self.clear = function () {
                    ServerLog.clearMessages(self.channel);
                    reset();
                };

                self.toggleScroll = function () {
                    self.autoScroll = !self.autoScroll;
                };

                self.toggleLevel = function (level) {
                    self.options.levels[level].enabled = !self.options.levels[level].enabled;
                    reset();
                    readMessages();
                };

                self.onRepeatDone = function () {
                    // trigger an auto scroll if the anchor was visible
                    if (self.autoScroll) {
                        // scroll to the last displayed alert
                        // use $timeout instead of $scope.$evalAsync to ensure the DOM has
                        // been rendered
                        // see http://blogs.microsoft.co.il/choroshin/2014/04/08/angularjs-postdigest-vs-timeout-when-dom-update-is-needed/
                        scrollToPromise = $timeout(function () {
                            $anchorScroll(self.anchorName);
                            logRepeatEndPending = false;
                            scrollToPromise = null;
                            maybeRefreshLog();
                        });
                    } else {
                        logRepeatEndPending = false;
                        maybeRefreshLog();
                    }
                };
            }
        ],
        bindings: {
            channel: '<'
        }
    });

},{"./vendor/jquery.visible":7}],5:[function(require,module,exports){
/**
 * Created by richard on 17/10/16.
 */

angular.module('bitcraft-server-log', ['bitcraft-faye']);

},{}],6:[function(require,module,exports){
/*global angular*/
'use strict';

angular.module('bitcraft-server-log')
    .factory('ServerLog', [
        '$rootScope', '$cookies', 'Faye',
        function ($rootScope, $cookies, Faye) {
            /** @type {Object.<string, ChannelConfig>} */
            var config = {};
            /** @type {Object.<string, Object>} */
            var subscriptions = {};
            /** @type {String} */
            var filterCookie = 'log_filter'; // log_filter=/channel1\|filter1\|filter2\_channel2\|filter1\|filter2...

            /**
             * @param {string} channel
             */
            function addSubscription(channel) {
                if (!$rootScope.messages) {
                    $rootScope.messages = {};
                }

                $rootScope.messages[channel] = [];
                subscriptions[channel] = Faye.subscribe(channel, function (context, message) {
                    /** @type ChannelConfig */
                    var channelConfig = config[channel];
                    var i;

                    // format
                    for (i = 0; i < channelConfig.formatList.length; i += 1) {
                        message = channelConfig.formatList[i](message);
                    }

                    // filter must return true to keep a message
                    for (i = 0; i < channelConfig.filterList.length; i += 1) {
                        if (!channelConfig.filterList[i](message)) {
                            return;
                        }
                    }

                    // push
                    $rootScope.messages[channel].push(message);
                    if ($rootScope.messages[channel].length > channelConfig.limit) {
                        $rootScope.messages[channel].shift();
                    }

                    // call the listeners
                    for (i = 0; i < channelConfig.listenerList.length; i += 1) {
                        channelConfig.listenerList[i](message);
                    }
                }, true);
            }

            /**
             * @param {string} channel
             */
            function createChannel(channel) {
                if (!config[channel]) {
                    addSubscription(channel);
                    config[channel] = {
                        filterList: [],
                        formatList: [],
                        displayFilterList: [],
                        listenerList: [],
                        limit: 1000,
                        options: {
                            levels: {
                                'SERVER_LOG.TRACE': {
                                    enabled: true,
                                    'background-color': '#dff0d8',
                                    'border-color': '#d0e9c6',
                                    color: '#3c763d'
                                },
                                'SERVER_LOG.DEBUG': {
                                    enabled: true,
                                    'background-color': '#a5f3e4',
                                    'border-color': '#62f5e7',
                                    color: '#4b89a7'
                                },
                                'SERVER_LOG.INFO': {
                                    enabled: true,
                                    'background-color': '#d9edf7',
                                    'border-color': '#bcdff1',
                                    color: '#31708f'
                                },
                                'SERVER_LOG.WARNING': {
                                    enabled: true,
                                    'background-color': '#fcf8e3',
                                    'border-color': '#faebcc',
                                    color: '#8a6d3b'
                                },
                                'SERVER_LOG.ERROR': {
                                    enabled: true,
                                    'background-color': '#f2dede',
                                    'border-color': '#ebcccc',
                                    color: '#a94442'
                                },
                                'SERVER_LOG.FATAL': {
                                    enabled: true,
                                    'background-color': '#3c3232',
                                    'border-color': '#756060',
                                    color: '#f3dada'
                                }
                            },
                            flushDelay: 200,
                            searchDelay: 200,
                            showStatus: true,
                            showClearMessages: true,
                            showAutoScroll: true,
                            showLevels: true,
                            showSearch: true,
                            showCustomFilter: true
                        }
                    };
                }
            }

            /**
             * @param {string} channel
             * @param {ChannelOptions} options
             */
            function setOptions(channel, options) {
                var key;
                if (!config[channel]) {
                    createChannel(channel);
                }

                for (key in config[channel].options) {
                    if (config[channel].options.hasOwnProperty(key) && options[key] !== undefined && options[key] !== null) {
                        config[channel].options[key] = options[key];
                    }
                }
            }

            /**
             * @param {string} channel
             * @returns {ChannelOptions}
             */
            function getOptions(channel) {
                return config[channel].options;
            }

            /**
             * @param {string} channel
             * @param {number} limit
             */
            function setLimit(channel, limit) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].limit = limit;
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             */
            function pushFilteringFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].filterList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeFilteringFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].filterList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].formatList[i], fn)) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].filterList[i] === fn) {
                            config[channel].filterList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object): object} fn
             */
            function pushFormatingFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].formatList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): object} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeFormatingFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].formatList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].formatList[i], fn)) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].formatList[i] === fn) {
                            config[channel].formatList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             */
            function pushDisplayFilterFunction(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].displayFilterList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object): boolean} fn
             * @param {function(function, function): boolean=} compares
             */
            function removeDisplayFilterFunction(channel, fn, compares) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].displayFilterList.length; i += 1) {
                    if (compares) {
                        if (compares(config[channel].displayFilterList[i], fn)) {
                            config[channel].displayFilterList.splice(i, 1);
                            return;
                        }
                    } else {
                        if (config[channel].displayFilterList[i] === fn) {
                            config[channel].displayFilterList.splice(i, 1);
                            return;
                        }
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {function(object)} fn
             */
            function onMessageReceived(channel, fn) {
                if (!config[channel]) {
                    createChannel(channel);
                }
                config[channel].listenerList.push(fn);
            }

            /**
             * @param {string} channel
             * @param {function(object)} fn
             */
            function offMessageReceived(channel, fn) {
                var i;
                if (!config[channel]) {
                    return;
                }

                for (i = 0; i < config[channel].listenerList.length; i += 1) {
                    if (config[channel].listenerList[i] === fn) {
                        config[channel].listenerList.splice(i, 1);
                        return;
                    }
                }
            }

            /**
             * @param {string} channel
             * @param {Object} message
             * @returns {boolean}
             */
            function isMessageVisible(channel, message) {
                var i;
                /** @type {DisplayFilterList} */
                var displayFilterList;

                if (!config[channel]) {
                    return true;
                }

                displayFilterList = config[channel].displayFilterList;

                for (i = 0; i < displayFilterList.length; i += 1) {
                    if (displayFilterList[i](message) === false) {
                        return false;
                    }
                }

                return true;
            }

            /**
             * @param {string} channel
             * @returns {Array<Object>}
             */
            function getMessages(channel) {
                if (!config[channel]) {
                    return null;
                }

                return $rootScope.messages[channel];
            }

            /**
             * @param {string} channel
             */
            function clearMessages(channel) {
                if (!config[channel]) {
                    return;
                }

                $rootScope.messages[channel] = [];
            }

            /**
             * Remove all subscriptions
             */
            function cleanUp() {
                var key;
                for (key in subscriptions) {
                    if (subscriptions.hasOwnProperty(key)) {
                        subscriptions[key].unsubscribe(true);
                    }
                }
            }

            /**
             * @param {Array<string>} e
             * @returns {Function}
             */
            function generateCustomFilterFunction(e) {
                return function (message) {
                    var i;
                    for (i = 1; i < e.length; i += 1) {
                        if (message.header.indexOf(e[i]) > -1 || (message.data && message.data.indexOf(e[i]) > -1)) {
                            return false;
                        }
                    }

                    return true;
                };
            }

            /**
             * @param {string=} channel
             */
            function updateCustomFiltering(channel) {
                var cookie = $cookies.get(filterCookie);
                if (cookie) {
                    cookie.split('\\_')
                        .map(function (e) {
                            return e.split('\\|');
                        })
                        .some(function (e) {
                            if (channel && channel !== e[0]) {
                                return false;
                            }

                            if (!config[e[0]]) {
                                createChannel(e[0]);
                            }

                            if (config[e[0]].customFilteringFunction) {
                                removeFilteringFunction(e[0], config[e[0]].customFilteringFunction);
                            }

                            config[e[0]].customFilteringFunction = generateCustomFilterFunction(e);
                            $rootScope.messages[e[0]] = $rootScope.messages[e[0]].filter(config[e[0]].customFilteringFunction);
                            pushFilteringFunction(e[0], config[e[0]].customFilteringFunction);
                            return true; //break
                        });
                }
            }

            updateCustomFiltering();

            return {
                createChannel: createChannel,
                setLimit: setLimit,
                setOptions: setOptions,
                getOptions: getOptions,
                updateCustomFiltering: updateCustomFiltering,

                pushFilteringFunction: pushFilteringFunction,
                removeFilteringFunction: removeFilteringFunction,

                pushFormatingFunction: pushFormatingFunction,
                removeFormatingFunction: removeFormatingFunction,

                pushDisplayFilterFunction: pushDisplayFilterFunction,
                removeDisplayFilterFunction: removeDisplayFilterFunction,

                onMessageReceived: onMessageReceived,
                offMessageReceived: offMessageReceived,

                isMessageVisible: isMessageVisible,

                getMessages: getMessages,
                clearMessages: clearMessages,

                cleanUp: cleanUp
            };
        }
    ]);

/**
 * @typedef {Array<function(object): boolean>} FilterList
 */

/**
 * @typedef {Array<function(object): object>} FormatList
 */

/**
 * @typedef {Array<function(object): boolean>} DisplayFilterList
 */

/**
 * @typedef {Array<function(object)>} ListenerList
 */

/**
 * @typedef {Object} ChannelOptions
 * @property {Array<string>} levels
 * @property {number} flushDelay
 * @property {number} searchDelay
 * @property {boolean} showStatus
 * @property {boolean} showClearMessages
 * @property {boolean} showAutoScroll
 * @property {boolean} showLevels
 * @property {boolean} showSearch
 * @property {boolean} showCustomFilter
 */

/**
 * @typedef {Object} ChannelConfig
 * @property {FilterList} filterList
 * @property {FormatList} formatList
 * @property {DisplayFilterList} displayFilterList
 * @property {ListenerList} listenerList
 * @property {Number} limit
 * @property {function|undefined} customFilteringFunction
 * @property {ChannelOptions} options
 */

},{}],7:[function(require,module,exports){
(function($){

    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *       the user visible viewport of a web browser.
     *       only accounts for vertical position, not horizontal.
     */
    var $w = $(window);
    $.fn.visible = function(partial,hidden,direction){

        if (this.length < 1)
            return;

        var $t        = this.length > 1 ? this.eq(0) : this,
            t         = $t.get(0),
            vpWidth   = $w.width(),
            vpHeight  = $w.height(),
            direction = (direction) ? direction : 'both',
            clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;

        if (typeof t.getBoundingClientRect === 'function'){

            // Use this native browser method, if available.
            var rec = t.getBoundingClientRect(),
                tViz = rec.top    >= 0 && rec.top    <  vpHeight,
                bViz = rec.bottom >  0 && rec.bottom <= vpHeight,
                lViz = rec.left   >= 0 && rec.left   <  vpWidth,
                rViz = rec.right  >  0 && rec.right  <= vpWidth,
                vVisible   = partial ? tViz || bViz : tViz && bViz,
                hVisible   = partial ? lViz || rViz : lViz && rViz;

            if(direction === 'both')
                return clientSize && vVisible && hVisible;
            else if(direction === 'vertical')
                return clientSize && vVisible;
            else if(direction === 'horizontal')
                return clientSize && hVisible;
        } else {

            var viewTop         = $w.scrollTop(),
                viewBottom      = viewTop + vpHeight,
                viewLeft        = $w.scrollLeft(),
                viewRight       = viewLeft + vpWidth,
                offset          = $t.offset(),
                _top            = offset.top,
                _bottom         = _top + $t.height(),
                _left           = offset.left,
                _right          = _left + $t.width(),
                compareTop      = partial === true ? _bottom : _top,
                compareBottom   = partial === true ? _top : _bottom,
                compareLeft     = partial === true ? _right : _left,
                compareRight    = partial === true ? _left : _right;

            if(direction === 'both')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
            else if(direction === 'vertical')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            else if(direction === 'horizontal')
                return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
        }
    };

})(jQuery);

},{}]},{},[1]);

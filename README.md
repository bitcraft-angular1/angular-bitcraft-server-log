# Components library for angular #

This component is a real time log viewer that rely on Faye and EventManager.

## Patch notes ##

Current version : 1.0.1.

### 1.0.1 ###

Fix a bug that caused to ignore the limit of messages.

## Configuration: ##

### Formating: ###

To set the formating of a message, you must write a function that takes an object as input.
This object match the one you gave to Faye.
This function must return an object with 3 fields, `header`, `level` and `data`.
Header expects a string. This will be the basic display of the message.
Level expects a string. It can be: `trace`, `debug`, `info`, `warning`, `error` or `fatal`.
Data expects an array of strings. If this array is not null, a `+` will be displayed on the log message to
display this additional information.

### Filtering: ###

To filter messages, you can use a filtering function. This function takes your message as an input and
must return a boolean. If the function return false, then the message is discarded.

### Displaying: ###

You can choose to hide some messages by setting a display function.
This function takes your message as an input and must return a boolean.
If the function return false, then the message is not shown.

### Limit: ###

You can specify a limit to the number of message to be stored. This limit is set by default to 1000.

## How to configure: ##

All of these parameters can be specified using the service `ServerLog` and by calling the following functions:

``` javascript
setFormatingFunction(channel, fn)
setFilteringFunction(channel, fn)
setDisplayFilterFunction(channel, fn)
setLimit(channel, limit)
setAll(channel, formatingFunction, filteringFunction, displayFilterFunction, limit)

```

## Sending a message to Faye ##

### Using Log4js ###

By using Log4js, it is super easy to send a message to the log component. In your configuration file, you just have to define an appender that
will send message on your channel.

``` javascript
appenders: [
    {type: 'console'},
    {
        type: 'log4js-faye-appender',
        url: 'http://localhost:2140/faye',
        channel: '/logChannel'
    }
],

```
